FROM node:18-bullseye-slim

WORKDIR /usr/src/app

RUN touch isReady.html

RUN npm install -g backstopjs@6.3.2;

# Chrome dependencies
RUN apt-get update && apt-get install -y \
    libnss3 \
    libatk1.0-0 \
    libatk-bridge2.0-0 \
    libcups2 \
    libdrm2 \
    libasound2 \
    libgbm1 \
    libxrandr2 \
    libxfixes3 \
    libxdamage1 \
    libpango1.0-0 \
    libxkbcommon0 \
    libxcomposite1 \
    procps \
    nginx \
    tree

RUN backstop init
RUN mkdir -p /usr/src/app/backstop_data/html_report
COPY nginx.conf /etc/nginx/nginx.conf

ENTRYPOINT ["nginx", "-g", "daemon off;"]
EXPOSE 80
