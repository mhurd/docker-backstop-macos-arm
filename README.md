Minimal Dockerized BackstopJS install demonstrating method enabling it to work on ARM macOS hosts

Spin it up from scratch via:

`./fresh_install`

The current issue is Puppeteer (used by Backstop) can't yet access Linux ARM binaries for Chrome:

https://github.com/GoogleChromeLabs/chrome-for-testing/issues/1

For now using `PLATFORM="--platform linux/amd64"` when building the image on ARM macOS hosts allows Rosetta to correctly work with the Chrome binary which gets retrieved when Backstop is installed 
